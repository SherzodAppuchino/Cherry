//
//  Fonts.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import UIKit

enum Fonts {
    case poppinsBlack
    case poppinsBlackItalic
    case poppinsBold
    case poppinsBoldItalic
    case poppinsExtraBold
    case poppinsExtraBoldItalic
    case poppinsExtraLight
    case poppinsExtraLightItalic
    case poppinsItalic
    case poppinsLight
    case poppinsLightItalic
    case poppinsMedium
    case poppinsMediumItalic
    case poppinsRegular
    case poppinsSemiBold
    case poppinsSemiBoldItalic
    case poppinsThin
    case poppinsThinItalic
}

extension Fonts {
    func ofSize(_ size: CGFloat) -> UIFont {
        switch self {
        case .poppinsBlack: return UIFont(name: "Poppins-Black", size: size)!
        case .poppinsBlackItalic: return UIFont(name: "Poppins-BlackItalic", size: size)!
        case .poppinsBold: return UIFont(name: "Poppins-Bold", size: size)!
        case .poppinsBoldItalic: return UIFont(name: "Poppins-BoldItalic", size: size)!
        case .poppinsExtraBold: return UIFont(name: "Poppins-ExtraBold", size: size)!
        case .poppinsExtraBoldItalic: return UIFont(name: "Poppins-ExtraBoldItalic", size: size)!
        case .poppinsExtraLight: return UIFont(name: "Poppins-ExtraLight", size: size)!
        case .poppinsExtraLightItalic: return UIFont(name: "Poppins-ExtraLightItalic", size: size)!
        case .poppinsItalic: return UIFont(name: "Poppins-Italic", size: size)!
        case .poppinsLight: return UIFont(name: "Poppins-Light", size: size)!
        case .poppinsLightItalic: return UIFont(name: "Poppins-LightItalic", size: size)!
        case .poppinsMedium: return UIFont(name: "Poppins-Medium", size: size)!
        case .poppinsMediumItalic: return UIFont(name: "Poppins-MediumItalic", size: size)!
        case .poppinsRegular: return UIFont(name: "Poppins-Regular", size: size)!
        case .poppinsSemiBold: return UIFont(name: "Poppins-SemiBold", size: size)!
        case .poppinsSemiBoldItalic: return UIFont(name: "Poppins-SemiBoldItalic", size: size)!
        case .poppinsThin: return UIFont(name: "Poppins-Thin", size: size)!
        case .poppinsThinItalic: return UIFont(name: "Poppins-ThinItalic", size: size)!
        }
    }
}
