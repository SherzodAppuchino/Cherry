//
//  Logger.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import Foundation

final class Logger {
    static func log(_ items: Any...) {
#if DEBUG
        print(items)
#endif
    }
}
