//
//  HomeViewController.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import UIKit
import SnapKit

class HomeViewController: UIViewController {
    
    // MARK: Properties
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register(SomeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.contentInset.top = NavigationBarController.Sizes.navigationBarLargeTitleView.height
        return collectionView
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarController()
        setupCollectionView()
    }
    
    // MARK: Private API
    
    private func setupNavigationBarController() {
        navigationBarController?.title = "Abra Kebabra"
    }
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension HomeViewController: UICollectionViewDelegate {    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationBarController?.handleVeriticalScroll(scrollView.contentOffset.y)
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: 100)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SomeCollectionViewCell
        cell.titleLabel.text = "Cell: #\(indexPath.row)"
        return cell
    }
}
