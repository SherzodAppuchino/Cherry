//
//  NavigationBarButton.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 09/12/22.
//

import UIKit
import SnapKit

class NavigationBarButton: UIControl {
    
    private let imageView: UIImageView
    
    override var isHighlighted: Bool {
        didSet {
            guard isHighlighted != oldValue else { return }
            animateAlphaChannel(isHighlighted)
        }
    }
    
    // MARK: Constructors
    
    required init(image: UIImage, frame: CGRect = .zero) {
        
        self.imageView = {
            let imageView = UIImageView()
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()
        
        super.init(frame: frame)
        
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private API
    
    private func setupViews() {
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func animateAlphaChannel(_ isHighlighted: Bool) {
        UIView.animate(withDuration: 0.1, delay: 0, options: [.allowUserInteraction, .curveEaseIn]) { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.imageView.alpha = weakSelf.isHighlighted ? 0.33 : 1
        }
    }
}
