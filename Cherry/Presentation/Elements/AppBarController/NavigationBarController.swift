//
//  NavigationBarController.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import UIKit
import SnapKit
import Combine

/// A container view controller that has a similar User interface as a native UINavigationController that has a NavigationBar, LargeTitleView, and ContentView.
/// The controller does not have its own stack navigation system. It will be correct to use it with a native UINavigationController with disabled NavgationBar
class NavigationBarController: UIViewController, UIGestureRecognizerDelegate {
    
    /// View that shows ContentViewController's title
    private let navigationBarView = NavigationBarView()
    
    /// View that shows ContentViewController's large title (aka Large Title View) which disappears when scrolling to the top
    private let navigationBarLargeTitleView = NavigationBarLargeTitleView()
    
    /// View that shows ContentViewController's view
    private let contentView = UIView()
    
    /// ContentViewController (child view controller) whose view will be shown in the contentView
    private let childViewController: UIViewController
    
    /// Property to store the last vertical point of the scrollView.
    /// When we get a vertical scroll changes in the **handleVeriticalScroll(\_:)** method, we write the Y point of the scroll to this property.
    /// If the value is less than zero, we won't change the Y position of the LargeTitleView on scroll.
    private var cachedVerticalVelocity: CGFloat?
    
    struct Sizes {
        static let navigationBarView = CGSize(width: .zero, height: 56)
        static let navigationBarLargeTitleView = CGSize(width: .zero, height: 56)
        static let navigationButton = CGSize(width: 24, height: 24)
    }
    
    // MARK: Constructors
    
    init(contentViewController: UIViewController, nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil) {
        self.childViewController = contentViewController
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.addChild(contentViewController)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRootView()
        setupNavigationBar()
        setupLargeTitleView()
        setupContentView()
        
        // We will configure InteractivePopGustureRecognizers differently on the main project
        //
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        updateNavigationBarConstraints()
    }
    
    // MARK: Private API
    
    private func updateNavigationBarConstraints() {
        navigationBarView.snp.updateConstraints { make in
            make.height.equalTo(view.safeAreaInsets.top + Sizes.navigationBarView.height)
        }
    }
    
    private func setupRootView() {
        view.backgroundColor = .white
    }
    
    private func setupNavigationBar() {
        view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.height.equalTo(Sizes.navigationBarView.height)
            make.leading.trailing.equalToSuperview()
        }

        navigationBarView.backButton.addTarget(self, action: #selector(didTapNavigateBack(_:)), for: .touchUpInside)
        navigationBarView.accountButton.addTarget(self, action: #selector(didTapAccount(_:)), for: .touchUpInside)

        let viewControllersCount = navigationController?.viewControllers.count ?? 0
        navigationBarView.setNavigateBackButtonVisible(viewControllersCount > 1)
    }
    
    @objc private func didTapNavigateBack(_ sender: UIControl) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func didTapAccount(_ sender: UIControl) {
        let controller = UIAlertController(title: "Oops", message: "message", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Close", style: .cancel)
        controller.addAction(cancelAction)
        present(controller, animated: true)
    }
    
    private func setupLargeTitleView() {
        view.insertSubview(navigationBarLargeTitleView, belowSubview: navigationBarView)
        navigationBarLargeTitleView.snp.makeConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(Sizes.navigationBarLargeTitleView.height)
        }
    }
    
    private func setupContentView() {
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setLargeNavgationTitleVisible(_ visible: Bool, _ animated: Bool = true) {
        navigationBarView.setNavgationTitleVisible(!visible, animated: animated)
        navigationBarLargeTitleView.setNavgationTitleVisible(visible, animated: animated)
    }
    
    private func setTitle(_ text: String?) {
        self.navigationBarView.setTitle(text)
        self.navigationBarLargeTitleView.setTitle(text)
    }
    
    // MARK: Public API
    
    /// The name for the controller that will be shown in the NavigationBarController
    override var title: String? {
        didSet {
            setTitle(title)
        }
    }
    
    /// A method for tracking the vertical change of scrollView offset
    /// Depending on the vertical position, we hide/show the large title on the NavigationBarController
    func handleVeriticalScroll(_ offset: CGFloat) {
        let offset = CGFloat.minimum(0, offset)
        let verticalVelocity = offset + Sizes.navigationBarLargeTitleView.height
        
        guard self.cachedVerticalVelocity != verticalVelocity else { return }
        self.cachedVerticalVelocity = verticalVelocity
        
        if verticalVelocity >= Sizes.navigationBarLargeTitleView.height * 0.45 {
            setLargeNavgationTitleVisible(false)
        } else {
            setLargeNavgationTitleVisible(true)
        }
        
        let positiveOffset: CGFloat = CGFloat.minimum(100, (offset + Sizes.navigationBarLargeTitleView.height) * -1)
        let fractionComplete: CGFloat = positiveOffset / 100
        navigationBarLargeTitleView.increaseFontSize(fractionComplete: fractionComplete)
        
        navigationBarLargeTitleView.snp.updateConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom).inset(verticalVelocity)
        }
    }
}
