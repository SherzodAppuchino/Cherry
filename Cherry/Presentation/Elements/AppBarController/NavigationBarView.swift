//
//  NavigationBarView.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import UIKit
import SnapKit

class NavigationBarView: UIView {
    
    private let contentView = UIView()
    
    let backButton: NavigationBarButton = NavigationBarButton(image: UIImage(named: "chevron.backward")!)
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.alpha = 0
        label.font = Fonts.poppinsMedium.ofSize(16)
        return label
    }()
    
    let accountButton: NavigationBarButton = NavigationBarButton(image: UIImage(named: "text_alignleft")!)
    
    // MARK: Constructors
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func safeAreaInsetsDidChange() {
        super.safeAreaInsetsDidChange()
        updateContentViewConstraints()
    }
    
    // MARK: Private API
    
    private func setupViews() {
        backgroundColor = .white
        
        addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(safeAreaInsets.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(NavigationBarController.Sizes.navigationBarView.height)
        }
        
        contentView.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.width.equalTo(NavigationBarController.Sizes.navigationButton.width)
            make.height.equalTo(NavigationBarController.Sizes.navigationButton.height)
            make.leading.equalToSuperview().inset(10)
            make.centerY.equalToSuperview()
        }
        
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        contentView.addSubview(accountButton)
        accountButton.snp.makeConstraints { make in
            make.width.equalTo(NavigationBarController.Sizes.navigationButton.width)
            make.height.equalTo(NavigationBarController.Sizes.navigationButton.height)
            make.trailing.equalToSuperview().inset(10)
            make.centerY.equalToSuperview()
        }
    }
    
    private func updateContentViewConstraints() {
        contentView.snp.updateConstraints { make in
            make.top.equalToSuperview().inset(safeAreaInsets.top)
        }
    }
    
    // MARK: Public API
    
    func setTitle(_ text: String?) {
        self.titleLabel.text = text
    }
    
    func setNavigateBackButtonVisible(_ visible: Bool, animated: Bool = true) {
        backButton.isEnabled = visible ? true : false
        
        if animated {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.backButton.alpha = visible ? 1 : 0
            }
        } else {
            backButton.alpha = visible ? 1 : 0
        }
    }
    
    func setNavgationTitleVisible(_ visible: Bool, animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.titleLabel.alpha = visible ? 1 : 0
            }
        } else {
            titleLabel.alpha = visible ? 1 : 0
        }
    }
}
