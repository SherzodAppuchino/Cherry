//
//  NavigationBarLargeTitleView.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 08/12/22.
//

import UIKit
import SnapKit

class NavigationBarLargeTitleView: UIView {
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Fonts.poppinsMedium.ofSize(28)
        return label
    }()
    
    // MARK: Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    // MARK: Public API
    
    func setTitle(_ text: String?) {
        self.titleLabel.text = text
    }
    
    func setNavgationTitleVisible(_ visible: Bool, animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.titleLabel.alpha = visible ? 1 : 0
            }
        } else {
            titleLabel.alpha = visible ? 1 : 0
        }
    }
    
    
    func increaseFontSize(fractionComplete: CGFloat) {
        let fractionComplete = CGFloat.minimum(1, CGFloat.maximum(0, fractionComplete))
        let additionalFontSize: CGFloat = 2 * fractionComplete
        titleLabel.font = Fonts.poppinsMedium.ofSize(28 + additionalFontSize)
    }
    
    // MARK: Private API
    
    private func setupViews() {
        backgroundColor = .white
        
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }
    }
}
