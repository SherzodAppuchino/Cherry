//
//  UIViewController+NavigationBarController.swift
//  Cherry
//
//  Created by Sherzod Akhmedov on 11/12/22.
//

import UIKit

extension UIViewController {
    
    /// The parent of the current controller, aka Navigation Bar Controller, inside which your controller is shown.
    /// This is an optional value. The value will only be returned if your controller is in NavigationBarController
    var navigationBarController: NavigationBarController? {
        return parent as? NavigationBarController
    }
}
